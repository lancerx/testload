# coding=utf-8
from locust import HttpLocust, TaskSet, task
from django.utils.encoding import smart_str, smart_text
import requests

# locust -f locustfile.py --host=http://127.0.0.1:8000


class WebsiteTasks(TaskSet):
    def on_start(self):
        self.client.get("/")

    @task
    def submit(self):
        response = self.client.get("/submit/")
        # self.client.get("/static/css/bootstrap.min.css")
        # self.client.get("/static/css/simple-sidebar.css")
        # self.client.get("/static/js/jquery-2.1.4.js")
        # self.client.get("/static/js/bootstrap.min.js")
        csrftoken = response.cookies['csrftoken']
        attach = open('file.pdf', 'rb')

        r = self.client.post("/submit/", data={
            'csrfmiddlewaretoken': csrftoken,
            'password': smart_str(u'wkefjgui'),
            'payload': smart_str(u'kjsdgfljdsh'),
            'commit': smart_str(u'Вкрапить / Embed'),
        }, files={'docfile': attach})


    @task
    def extract(self):
        response = self.client.get("/extract/")
        csrftoken = response.cookies['csrftoken']
        attach = open('file.pdf', 'rb')

        r = self.client.post("/extract/", {
            'csrfmiddlewaretoken': csrftoken,
            'password': smart_str(u'wkefjgui'),
            'commit': smart_str(u'Извлечь / Extract'),
        }, files={'docfile': attach})
        print(r.text)
        file = 'response.html'

        with open(file, 'w') as f:
            data = smart_str(r.text)
            f.write(data)
            f.close()


class WebsiteUser(HttpLocust):
    task_set = WebsiteTasks
    min_wait = 5000
    max_wait = 15000


